package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"github.com/gocolly/colly"
	"gitlab.com/therealkevinard/neuco-scraper/NeucoScraper"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
	"time"
)

var outputFormat *string

var domain = "http://www.neuco.com/"
var baseUrl = domain + "ecommerce/"

var commonCollector *colly.Collector
var detailCollector *colly.Collector

// load command-line flage
func loadArgs() {
	outputFormat = flag.String("output", "log", "output format (log|json)")
	flag.Parse()
}

// setup boilerplates the *colly.Collector instances we use later
func setup() {
	commonCollector = colly.NewCollector()
	commonCollector.Limit(&colly.LimitRule{
		DomainGlob: "neuco.com/*",
	})

	//-- login
	err := commonCollector.Post(baseUrl+"LoginAction.action", map[string]string{"loginId": "Haglerllc", "password": "Pumpman28", "myname": "Bob Sanders"})
	if err != nil {
		log.Fatal(err)
	}

	//-- handle request
	commonCollector.OnRequest(func(r *colly.Request) {
		fmt.Println("Visiting", r.URL)
	})

	//-- handle response
	commonCollector.OnResponse(func(r *colly.Response) {
		fmt.Println("Visited", r.Request.URL)
	})

	detailCollector = commonCollector.Clone()

	//-- received html.
	detailCollector.OnHTML("body", func(e *colly.HTMLElement) {
		start := time.Now()
		// basic product data in div#details
		product := NeucoScraper.Product{
			OriginalUrl:  e.Request.URL.String(),
			Manufacturer: e.ChildText("#manufacturer > span"),
			PartNumber:   e.ChildText("#part-num"),
			Description:  e.ChildText("#keywords > #descripiton"),
			StockStatus:  e.ChildText("#stock-status"),
			StockLevel:   e.ChildText("#availability-msg > span:nth-child(2)"),
			Literature:   make(map[string]string),
			ShippingData: make(map[string]string),
		}
		singlePrice := NeucoScraper.SinglePrice{
			Type:  e.ChildText("#part-price .price-label"),
			Value: e.ChildText("#part-price .price-number"),
		}
		product.Pricing = append(product.Pricing, singlePrice)

		// deeper data in div#detail-tabs
		// literature
		e.ForEach("#technical p a", func(_ int, el *colly.HTMLElement) {
			if el.Attr("href") == "#" {
				return
			}
			product.Literature[el.Text] = el.Attr("href")
		})
		// shipping data
		e.ForEach("#shipping table tr", func(_ int, el *colly.HTMLElement) {
			product.ShippingData[el.ChildText("td:nth-child(1)")] = el.ChildText("td:nth-child(2)")
		})

		elapsed := time.Since(start)

		b, _ := json.Marshal(product)

		switch *outputFormat {
		case "log":
			log.Printf("time: %s\toutput: %s", elapsed, string(b))
		default:
			fmt.Println(string(b))
		}
	})
}

func LoadCategories() NeucoScraper.CategoriesResponse {
	url := "http://www.neuco.com/ecommerce/CategoryJsonAction.action"
	categoriesClient := http.Client{
		Timeout: time.Second * 5,
	}

	req, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		log.Fatal(err)
	}

	req.Header.Set("User-Agent", "category-fetch")
	res, err := categoriesClient.Do(req)
	if err != nil {
		log.Fatal(err)
	}

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		log.Fatal(err)
	}

	dataset := NeucoScraper.CategoriesResponse{}
	jsonErr := json.Unmarshal(body, &dataset)
	if jsonErr != nil {
		log.Fatal(jsonErr)
	}

	return dataset
}

func searchForm(queryString string) {
	err := commonCollector.Post(baseUrl+"Partsearch3Action.action", map[string]string{
		"searchchoice": "keyword",
		"partid":       queryString,
	})
	if err != nil {
		log.Fatal(err)
	}
}

func bruteForcePids() {
	pidRange := map[string]int{
		"start": 0,
		"end":   1300000,
	}

	i := pidRange["start"]
	for i < pidRange["end"] {
		i += 1
		url := baseUrl + "PartdetailAction.action?partid=" + strconv.Itoa(i)
		detailCollector.Visit(url)
	}
}

func main() {
	loadArgs()
	setup()
	bruteForcePids()
}
