package NeucoScraper

import "strings"

type ProductCat struct {
	Id   int    `json:"id"`
	Main string `json:"main_category"`
	Sub  string `json:"sub_category"`
}

/*
QueryString gets sanitized query string from product categories
*/
func (pc *ProductCat) QueryString() string {
	qs := pc.Main + " " + pc.Sub
	qs = strings.Replace(qs, "_", " ", -1)
	qs = strings.Replace(qs, "-", " ", -1)
	qs = strings.Replace(qs, "/", " ", -1)
	return qs
}

func (pc *ProductCat) IsReal() bool {
	return strings.Index(pc.Main, "test") == -1
}

type CategoriesResponse struct {
	Status      string       `json:"JSON"`
	MainCats    []string     `json:"maincategorys"`
	ProductCats []ProductCat `json:"pcategorys"`
}
