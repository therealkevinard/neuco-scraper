package NeucoScraper

type SinglePrice struct {
	Type  string `json:"type"`
	Value string `json:"price"`
}

type Product struct {
	OriginalUrl  string        `json:"url"`
	Manufacturer string        `json:"manufacturer"`
	PartNumber   string        `json:"part_number"`
	PartId       int           `json:"part_id"`
	Description  string        `json:"description"`
	StockStatus  string        `json:"stock_status"`
	StockLevel   string        `json:"stock_level"` // @todo int
	Pricing      []SinglePrice `json:"price"`

	Literature   map[string]string `json:"literature"`
	ShippingData map[string]string `json:"shipping_data"`
}
